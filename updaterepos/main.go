package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"sync"
)

// update master branches & dependencies by executing go get
func main() {
	root := os.Getenv("GOPATH") + "/src/git.speu.io/"
	var wg sync.WaitGroup

	// libraries
	librariesPath := root + "golib/"
	files, _ := ioutil.ReadDir(librariesPath)
	for _, file := range files {
		if file.IsDir() {
			go goGet(librariesPath+file.Name(), &wg)
		}
	}
	wg.Wait()

	// services
	servicesPath := root + "usvc/"
	files, _ = ioutil.ReadDir(servicesPath)
	for _, file := range files {
		if file.IsDir() {
			go goGet(servicesPath+file.Name(), &wg)
		}
	}

	// feeds
	feedsPath := root + "feed-processing/"
	files, _ = ioutil.ReadDir(feedsPath)
	for _, file := range files {
		if file.IsDir() {
			go goGet(feedsPath+file.Name(), &wg)
		}
	}

	// ui
	// don't do 'go get' on UI (no go dependecies)
	go gitPull(root+"ui/admin", &wg)
	go gitPull(root+"ui/frontend", &wg)

	wg.Wait()

	// ui repos have git submodules
	go gitSubmodules(root+"ui/admin", &wg)
	go gitSubmodules(root+"ui/frontend", &wg)

	wg.Wait()
}

// perform git pull + "go get" for the path
func goGet(path string, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()

	gitPull(path, wg)

	// go get
	params := []string{"get"}
	cmd := exec.Command("go", params...)
	cmd.Dir = path
	err := cmd.Run()
	if err != nil {
		logError(path)
	}
}

func gitPull(path string, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()

	// git checkout
	params := []string{"checkout", "master"}
	cmd := exec.Command("git", params...)
	cmd.Dir = path
	_ = cmd.Run()

	// git pull
	params = []string{"pull"}
	cmd = exec.Command("git", params...)
	cmd.Dir = path
	_ = cmd.Run()
}

// git submodule update
func gitSubmodules(path string, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()

	params := []string{"submodule", "update"}
	cmd := exec.Command("git", params...)
	cmd.Dir = path
	_ = cmd.Run()
}

func logError(path string) {
	ignorePaths := []string{"ui/frontend", "usvc/db-design", "usvc/utils", "ui/admin", "/.vscode", "usvc/dev-local", "usvc/mysql", "golib/go-micro", "feed-processing/libs"}
	if strings.HasSuffix(path, ignorePaths[0]) ||
		strings.HasSuffix(path, ignorePaths[1]) ||
		strings.HasSuffix(path, ignorePaths[2]) ||
		strings.HasSuffix(path, ignorePaths[3]) ||
		strings.HasSuffix(path, ignorePaths[4]) ||
		strings.HasSuffix(path, ignorePaths[5]) ||
		strings.HasSuffix(path, ignorePaths[6]) ||
		strings.HasSuffix(path, ignorePaths[7]) ||
		strings.HasSuffix(path, ignorePaths[8]) {
		return
	}
	println("problem in: " + path)
}
